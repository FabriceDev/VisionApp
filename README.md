# VisionApp

### Notes

The VisionApp's goal is to detect faces in picture. It allows user to draw the faces and their features, as well as adding element like a red nose. Users get the possibility to save their pictures customize with Vision into their gallery.

While being accurate most of the time, [some users have complain](https://developer.apple.com/forums/thread/690605) about issues with Xcode Simulators and the Vision's Framework. This is why I encourage you to try the VisionApp directly from a device too.