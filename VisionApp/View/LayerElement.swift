//
//  LayerElement.swift
//  VisionApp
//
//  Created by Fabrice M. on 18/08/2021.
//

import UIKit

class LayerElement: CAShapeLayer {
    
    init(_ rect: CGRect) {
        super.init()
        self.setupElement(.red)
        
        let path = UIBezierPath(rect: rect)
        
        self.path = path.cgPath
    }
    
    init(_ points: [CGPoint]) {
        super.init()
        self.setupElement(.cyan)
        
        let path = UIBezierPath()
        guard let first = points.first else { return }
        
        path.move(to: first)
        points.forEach { point in
            path.addLine(to: point)
            path.move(to: point)
        }
        path.addLine(to: first)
        self.path = path.cgPath
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupElement(_ color: UIColor) {
        self.fillColor = UIColor.clear.cgColor
        self.strokeColor = color.cgColor
        self.lineWidth = 2
    }
}
