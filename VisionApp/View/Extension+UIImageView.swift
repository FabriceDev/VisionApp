//
//  Extension+UIImageView.swift
//  VisionApp
//
//  Created by Fabrice M. on 17/08/2021.
//

import UIKit
import Vision

extension UIImageView {
    
    func deleteVision(type: TypeVision) {
        switch type {
        case .face, .landmarks:
            if self.layer.sublayers?.filter({ $0 is LayerElement }).first != nil {
                self.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
            } else {
                self.subviews.forEach({ $0.removeFromSuperview() })
            }
        case .redNose:
            self.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
        }
    }
    
    func imageRect() -> CGRect {
        let imageSize = self.image?.size ?? CGSize(width: 0, height: 0)
        let width = self.frame.width
        let height = width / imageSize.width * imageSize.height
        let y = self.frame.height / 2 - height / 2
        return CGRect(x: 0, y: y, width: width, height: height)
    }

    func imagePoints(_ points: [CGPoint], _ face: CGRect) -> [CGPoint] {
        var pointsConverted = [CGPoint]()
        let box = self.faceRect(face)
        let height = self.imageRect().height * face.height
        
        points.forEach { point in
            let x = point.x * box.width + box.origin.x
            let y = (point.y * (1 - box.height)) + box.origin.y + height
            pointsConverted.append(CGPoint(x: x, y: y))
        }
        return pointsConverted
    }
    
    func faceRect(_ face: CGRect) -> CGRect {
        let imgRect = self.imageRect()
        let x = imgRect.width * face.origin.x + imgRect.minX
        let width = imgRect.width * face.width
        let height = imgRect.height * face.height
        let y = (imgRect.height * (1 - face.origin.y) - height) + imgRect.minY
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func faceLandmarks(_ face: VNFaceObservation) {
        if let landmarks = face.landmarks {
            let points = landmarks.getPoints()
            points.forEach { point in
                let drawing = self.imagePoints(point, face.boundingBox)
                self.layer.addSublayer(LayerElement(drawing))
            }
        }
    }
    
    func faceRedNose(_ face: VNFaceObservation) {
        if let landmarks = face.landmarks, let nose = landmarks.nose {
            let nosePoints = self.imagePoints(nose.normalizedPoints, face.boundingBox)
            if let first = nosePoints.first {
                var minX = first.x
                var maxX = first.x
                var minY = first.y
                var maxY = first.y
                
                nosePoints.forEach { (p) in
                    minX = (p.x < minX) ? p.x : minX
                    maxX = (p.x > maxX) ? p.x : maxX
                    minY = (p.y < minY) ? p.y : minY
                    maxY = (p.y > maxY) ? p.y : maxY
                }
                
                let size = maxX - minX
                let rect = CGRect(x: minX, y: maxY - size, width: size, height: size)
                let noseImg = UIImageView(frame: rect)
                
                noseImg.image = UIImage(named: "redNose")
                self.addSubview(noseImg)
            }
        }
    }
}
