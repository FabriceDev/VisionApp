//
//  Extension+VNFaceLandmarks2D.swift
//  VisionApp
//
//  Created by Fabrice M. on 17/08/2021.
//

import UIKit
import Vision

extension VNFaceLandmarks2D {
    
    func getPoints() -> [[CGPoint]] {
        var faceElements = [[CGPoint]]()
        if let nose = self.nose {
            faceElements.append(nose.normalizedPoints)
        }
        if let contour = self.faceContour {
            faceElements.append(contour.normalizedPoints)
        }
        if let leftEye = self.leftEye {
            faceElements.append(leftEye.normalizedPoints)
        }
        if let rightEye = self.rightEye {
            faceElements.append(rightEye.normalizedPoints)
        }
        return faceElements
    }
}
