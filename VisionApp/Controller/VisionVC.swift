//
//  VisionVC.swift
//  VisionApp
//
//  Created by Fabrice M. on 17/08/2021.
//

import UIKit
import Vision

class VisionVC: UIViewController {
    
    @IBOutlet weak var visionSegmented: UISegmentedControl!
    @IBOutlet weak var visionSelectedImage: UIImageView!
    
    var image: UIImage?
    var visionType: TypeVision = .face
    var visionFaceObservation: [[VNFaceObservation]] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visionSelectedImage.image = image
        performVision()
    }
    

    func performVision() {
        if let cgiImage = image?.cgImage {
            let handler = VNImageRequestHandler(cgImage: cgiImage, options: [:])
            DispatchQueue.global(qos: .userInitiated).async {
                do {
                    try handler.perform(self.visionType.requests(self.visionCompletion))
                } catch {
                    AlertHelper.alert(error.localizedDescription, self)
                }
            }
        } else {
            AlertHelper.alert("Image non convertible", self)
        }
    }
    
    func visionCompletion(_ request: VNRequest, _ error: Error?) {
        DispatchQueue.main.async {
            if error != nil {
                AlertHelper.alert(error!.localizedDescription, self)
            } else if let results = request.results as? [VNFaceObservation] {
                if !results.isEmpty {
                    self.visionFaceObservation.append(results)
                }
                if request is VNDetectFaceRectanglesRequest {
                    self.addVisionToImage(self.visionFaceObservation)
                }
            }
        }
    }
    
    func addVisionToImage(_ visionFace: [[VNFaceObservation]?]) {
        if visionFace.first != nil,  let faces = visionFace[visionType.rawValue] {
            for face in faces {
                visionType.perform(face, self.visionSelectedImage)
            }
        } else {
            AlertHelper.alert("Visage non reconnu", self)
        }
    }
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let err = error {
            AlertHelper.alert(err.localizedDescription, self)
        } else {
            AlertHelper.alert("\n Votre photo a bien été enregistrée dans la librairie photo", self, "Sauvegarde réussi")
        }
    }
    
    
    @IBAction func visionSegmentedPressed(_ sender: Any) {
        visionType = TypeVision(rawValue: self.visionSegmented.selectedSegmentIndex) ?? .face
        self.visionSelectedImage.deleteVision(type: visionType)
        addVisionToImage(visionFaceObservation)
    }
    
    @IBAction func savePressed(_ sender: Any) {
        guard var image = self.visionSelectedImage.image else {
            AlertHelper.alert("Image non trouvé", self)
            return
        }

        UIGraphicsBeginImageContext(self.visionSelectedImage.bounds.size)
        self.visionSelectedImage.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
}
