//
//  HomeVC.swift
//  VisionApp
//
//  Created by Fabrice M. on 17/08/2021.
//

import UIKit

class HomeVC: UIViewController {

    @IBOutlet weak var visionImage: UIImageView!
    @IBOutlet weak var visionButton: UIButton!
    @IBOutlet weak var visionLabel: UILabel!
    
    var picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visionButton.isEnabled = visionImage.image != nil
        visionImage.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(takePhoto)))
        
        picker.delegate = self
        picker.allowsEditing = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !picker.isBeingDismissed {
            visionImage.image = nil
            visionButton.isEnabled = false
            visionLabel.isHidden = false
        }
    }

    @objc func takePhoto() {
        AlertHelper.alertImage(picker, self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueVision" {
            if let destination = segue.destination as? VisionVC {
                destination.image = sender as? UIImage
            }
        }
    }
    
    @IBAction func visionButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "segueVision", sender: visionImage.image)
    }
}

extension HomeVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        visionImage.image = info[.originalImage] as? UIImage

        visionButton.isEnabled = visionImage.image != nil
        visionLabel.isHidden = visionImage.image != nil
        
        picker.dismiss(animated: true, completion: nil)
    }
}
