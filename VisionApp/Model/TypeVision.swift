//
//  TypeVision.swift
//  VisionApp
//
//  Created by Fabrice M. on 18/08/2021.
//

import UIKit
import Vision

enum TypeVision: Int {
    case face = 0
    case landmarks = 1
    case redNose = 2
    
    func requests(_ completion: VNRequestCompletionHandler?) -> [VNRequest] {
        return [VNDetectFaceRectanglesRequest(completionHandler: completion), VNDetectFaceLandmarksRequest(completionHandler: completion), VNDetectFaceLandmarksRequest(completionHandler: completion)]
    }
    
    func perform(_ face: VNFaceObservation, _ image: UIImageView) {
        switch self {
        case .face:
            let rect = image.faceRect(face.boundingBox)
            image.layer.addSublayer(LayerElement(rect))
        case .landmarks:
            image.faceLandmarks(face)
        case .redNose:
            image.faceRedNose(face)
        }
    }
}
