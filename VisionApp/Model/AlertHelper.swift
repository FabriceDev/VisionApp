//
//  AlertHelper.swift
//  VisionApp
//
//  Created by Fabrice M. on 17/08/2021.
//

import UIKit

class AlertHelper {
    
    static func alert(_ message: String, _ controller: UIViewController, _ title: String = "Erreur") {
        
        let alertC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alertC.addAction(ok)
        controller.present(alertC, animated: true, completion: nil)
    }
    
    static func alertImage(_ picker: UIImagePickerController, _ controller: UIViewController) {
        
        let alertC = UIAlertController(title: "Prendre une photo", message: "Quel type de média souhaitez vous utiliser ?", preferredStyle: .alert)
        let photo = UIAlertAction(title: "Appareil photo", style: .default) { action in
            self.checkImageSource(picker, controller, .camera)
        }
        let gallery = UIAlertAction(title: "Librairie photo", style: .default) { action in
            self.checkImageSource(picker, controller, .photoLibrary)
        }
        let cancel = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        
        alertC.addAction(photo)
        alertC.addAction(gallery)
        alertC.addAction(cancel)
        controller.present(alertC, animated: true, completion: nil)
    }
    
    static func checkImageSource(_ picker: UIImagePickerController, _ controller: UIViewController, _ source: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(source) {
            picker.sourceType = source
            controller.present(picker, animated: true, completion: nil)
        } else {
            AlertHelper.alert("Média non disponible", controller)
        }
    }
}
